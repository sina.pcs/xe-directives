﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {

            @Foundation.Core.DirectiveDependency({ name: 'xeposMultiSelect' })
            export class xeposMultiSelect implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'AE',
                        scope: { repeat: '=', propertyToShow: '@', ngModel: '=', label: '@', modelProperty: '@', name: '@', ngDisabled: '=', modelType: '@', useFirstAsAll:'=' },
                        template: function (elem, attrs:any) {
                            return `<div class="xepos-input" ng-class="{'md-input-has-value':selected != undefined , 'not-valid':(${attrs.ngRequired} && !ngModel)}">
                                        <label>{{label}}</label>
                                        <input ng-if="!allSelected" autocomplete="off" ng-model="search" ng-blur="closeDropdown()" ng-click="openDropdown()" ng-focus="openDropdown()" ng-disabled="ngDisabled" type="text" />
                                        <input ng-if="allSelected" autocomplete="off" ng-model="search" ng-blur="closeDropdown()" ng-click="openDropdown()" ng-focus="openDropdown()" ng-disabled="ngDisabled" type="text" placeholder="{{tags[0][propertyToShow]}}" />
                                        <div class="dropdown" ng-class="{open:open}" ng-click="focusOnInput()">
                                            <span class="sprite sprite-dropdown-icon"></span>
                                        </div>                                        
                                    </div>
                                    <div class="tags" ng-if="!allSelected"> 
                                        <span class="xepos-chip" ng-repeat="tag in tags track by $index">{{tag[propertyToShow]}}<button ng-click="deleteTag(tag)" ng-disabled="ngDisabled" class="sprite sprite-chip-X"></button></span>
                                    </div>
                                   
                                    <div ng-show="open" class="xepos-select-container" ng-class="{reverse:reverseOpen}" keyboard-index="{{keyboardIndex}}">
                                        <md-button ng-repeat="obj in repeat | filter : search" ng-mousedown="selectThis(obj)" ng-class="{'keyboard-over':$index==keyboardIndex}">
                                            {{obj[propertyToShow]}}
                                        </md-button>
                                    </div>`
                        },
                        link: function (scope: any, element: JQuery, attr: ng.IAttributes, ngModel: any) {

                            scope.open = false;
                            scope.keyboardIndex = -1;
                            if (scope.modelProperty == undefined)
                                scope.modelProperty = 'Id';
                            if (scope.propertyToShow == undefined)
                                scope.propertyToShow = 'Name';

                            let dependencyManager = Foundation.Core.DependencyManager.getCurrent();
                            let $timeout = dependencyManager.resolveObject<ng.ITimeoutService>("$timeout");

                            let oldSelect;
                            scope.tags = [];
                            scope.search = "";

                            //$timeout(() => {

                            scope.$watch(function () { return scope.repeat }, function (repeat) {
                                if (ngModel.$viewValue)
                                    scope.findTagsInArray(ngModel.$viewValue);
                            })

                            let unRegister = scope.$watch(function () { return ngModel.$viewValue }, function (model) {
                                if (scope.repeat)
                                    scope.findTagsInArray(model);

                                //unRegister();

                                //if (!scope.New) {
                                //    model.map(tagId => {
                                //        scope.repeat.map(tag => {
                                //            if (tag[scope.modelProperty] == tagId) {
                                //                scope.tags.push(tag);
                                //            }
                                //        });
                                //    });

                                //}
                            });
                            //});

                            scope.findTagsInArray = function (model){
                                scope.tags = [];

                                if (model == undefined || model == "" || model == null) {
                                    return;
                                }

                                else {

                                    if (scope.modelType == undefined)
                                        model.map(tagId => {
                                            if (scope.repeat)
                                                scope.repeat.map(tag => {
                                                    if (tag[scope.modelProperty] == tagId) {
                                                        scope.tags.push(tag);
                                                    }
                                                });
                                        });
                                    else
                                        model.map(tag => {
                                            scope.tags.push(tag);
                                        });


                                    scope.allSelected = scope.useFirstAsAll && scope.tags[0][scope.modelProperty] == scope.repeat[0][scope.modelProperty] ? true : false;

                                }
                            }

                            scope.selectThis = function (obj: Object, elementExistsInRepeat = true) {
                                scope.selected = obj;
                                scope.search = "";
                                
                                let temp = [];
                                scope.allSelected = false;

                                if (ngModel.$modelValue) {

                                    if (!scope.useFirstAsAll || (obj[scope.modelProperty] != scope.repeat[0][scope.modelProperty] && ngModel.$modelValue[0] != scope.repeat[0][scope.modelProperty])) {
                                        temp = angular.copy(ngModel.$modelValue);
                                    }
                                }

                                if (scope.useFirstAsAll && obj[scope.modelProperty] == scope.repeat[0][scope.modelProperty]){
                                    scope.allSelected = true;
                                }
                                //if (temp.findIndex(obj[scope.modelProperty]))
                                //    return;

                                let elementExistsInModel = false;

                                if (scope.modelType == undefined) {
                                    temp.forEach(item => {
                                        if (obj[scope.modelProperty] == item) {
                                            elementExistsInModel = true;
                                            return;
                                        }
                                    });
                                }
                                else
                                {
                                    temp.forEach(item => {
                                        if (obj[scope.modelProperty] == item[scope.modelProperty]) {
                                            elementExistsInModel = true;
                                            return;
                                        }
                                    });
                                }
                                
                                if (elementExistsInModel)
                                    return;

                                if (scope.modelType == undefined)
                                    temp.push(obj[scope.modelProperty]);
                                else
                                    temp.push(obj);

                                ngModel.$setViewValue(temp);
                                scope.tags.push(obj);

                                if (!elementExistsInRepeat)
                                    scope.repeat.push(obj);

                                oldSelect = scope.search;
                                scope.open = false;
                            }

                            scope.focusOnInput = function () {
                                element.children('.xepos-input').children('input').focus();
                            }

                            scope.openDropdown = function () {
                                scope.open = true;
                                scope.search = "";
                                scope.keyboardIndex = -1;
                                let topOffset = element.offset().top;
                                //if (topOffset + 300 > window.innerHeight) {
                                //    scope.reverseOpen = true;
                                //}
                            }

                            scope.closeDropdown = function () {
                                setTimeout(() => {
                                    scope.$apply(function () {
                                        scope.open = false;
                                        scope.search = "";
                                    });
                                }, 100);
                            }

                            scope.deleteTag = function (obj: Object) {
                                let index = scope.tags.indexOf(obj);
                                scope.tags.splice(index, 1);

                                let temp = angular.copy(ngModel.$modelValue);
                                index = temp.indexOf(obj[scope.modelProperty]);
                                temp.splice(index, 1);
                                ngModel.$setViewValue(temp);
                            }


                            element.on('keydown', function (event: any) {
                                if (scope.open && event.key == 'Enter')
                                    event.preventDefault();
                                switch (event.key) {
                                    case 'ArrowDown':
                                        if (scope.keyboardIndex < scope.repeat.length - 1)
                                            scope.keyboardIndex++;
                                        if (!scope.open)
                                            scope.openDropdown();
                                        break;

                                    case 'ArrowUp':
                                        if (scope.keyboardIndex > 0) scope.keyboardIndex--;
                                        break;

                                    case 'Enter':
                                        if (scope.keyboardIndex > -1)
                                            scope.selectThis(scope.repeat[scope.keyboardIndex]);
                                        break;

                                    default:
                                        scope.keyboardIndex = -1;
                                        break;
                                }
                            });
                        }
                    });
                }
            }
        }
    }
}
