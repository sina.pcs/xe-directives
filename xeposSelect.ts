﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {

            @Foundation.Core.DirectiveDependency({ name: 'xeposSelect' })
            export class xeposSelect implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'AE',
                        scope: { repeat: '=', ngModel: '=', propertyToShow: '@', label: '@', modelProperty: '@', hasNewButton: '@', newButton: '&', color: '@', ngDisabled: '=', clearable: '=?', editable: '=?', modelType: '@', hasPaging: '@', onPageChange: '&',currentPageNumber:'@',lastPageNumber:'@' },
                        template: function (elem, attrs: any) {
                            return `<div class="xepos-input" ng-class="{'md-input-has-value':selected != undefined,'not-valid':(${attrs.ngRequired} && !ngModel)}">
                                        <label>{{label}}</label>
                                        <input autocomplete="off" ng-class="{'has-color':color}" ng-disabled="ngDisabled" ng-model="search" ng-blur="closeDropdown($event)" ng-click="openDropdown()" ng-focus="openDropdown()" type="text"  />
                                        <div ng-if="clearable" class="clear" ng-show="!open && search" ng-click="clear()">
                                            <span class="sprite sprite-clear-icon"></span>
                                        </div>                                         
                                        <div class="dropdown" ng-class="{open:open}" ng-click="focusOnInput()">
                                            <span class="sprite sprite-dropdown-icon"></span>
                                        </div>                                            
                                        <div class="color-in-input" ng-click="focusOnInput()" style="background-color:{{ngModel}}" ng-if="color"></div>                                     
                                    </div>
                                    <div ng-if="open" class="xepos-select-container" ng-class="{reverse:reverseOpen,'no-label':label==''}" keyboard-index="{{keyboardIndex}}">
                                        <div style="max-height:300px; overflow:auto">
                                            <div class="loading" ng-if="loading"><div class="spinner"></div></div>

                                            <md-button ng-repeat="obj in repeat | filter : search track by $index" ng-mousedown="selectThis($event,obj)" ng-class="{'keyboard-over':$index==keyboardIndex,'color-item':obj.Color}" style="background:{{obj.Color}}">
                                                <span class="color" style="background-color:{{obj[modelProperty]}}" ng-if="color"/>{{obj[propertyToShow]}}
                                                <md-button class="edit-btn" ng-if="editable">Edit</md-button>
                                            </md-button>
                                            <md-button ng-if="hasNewButton" class="new-btn" ng-mousedown="newButton()">
                                                <span class="sprite sprite-blue-plus" />Add new {{label}}
                                            </md-button>

                                            <div ng-if="hasPaging" class="paging">
                                                <md-button ng-disabled="currentPageNumber == 1" class="previous-page" ng-click="previousPage()"><span class="sprite sprite-arrow-left-icon"></span></md-button>
                                                <div class="page-number">{{currentPageNumber}}</div>
                                                <md-button ng-disabled="currentPageNumber == lastPageNumber" class="next-page" ng-click="$event.stopImmediatePropagation(); nextPage()"><span class="sprite sprite-arrow-right-icon"></span></md-button>
                                            </div>
                                        </div>
                                    </div>`
                        },
                        link: function (scope: any, element: JQuery, attr: ng.IAttributes, ngModel: ng.INgModelController) {

                            scope.loading = false;
                            scope.currentPageNumber = scope.currentPageNumber ? parseInt(scope.currentPageNumber) : 1;

                            if (scope.clearable == undefined)
                                scope.clearable = true;

                            scope.open = false;
                            scope.keyboardIndex = -1;
                            if (scope.modelProperty == undefined)
                                scope.modelProperty = 'Id';
                            if (scope.propertyToShow == undefined)
                                scope.propertyToShow = 'Name';

                            let dependencyManager = Foundation.Core.DependencyManager.getCurrent();
                            let $timeout = dependencyManager.resolveObject<ng.ITimeoutService>("$timeout");

                            let oldSelect;
                            scope.$watch(function () { return ngModel.$viewValue }, function (model) {
                                //model = model.toString();
                                if (model == undefined || model == "" || model == null) {
                                    scope.search = "";
                                    oldSelect = scope.search;
                                    return;
                                }



                                $.each(scope.repeat, function (key, value) {
                                    if (value[scope.modelProperty] == model) {
                                        scope.search = value[scope.propertyToShow];
                                        oldSelect = scope.search;
                                    }
                                })
                            });

                            scope.clear = function () {
                                scope.search = "";
                                ngModel.$setViewValue("");
                                oldSelect = scope.search;
                                scope.open = false;
                            }
                            scope.selectThis = function (e: JQueryEventObject, obj: Object) {
                                if (angular.element(e.target).hasClass('edit-btn')) {
                                    scope.newButton({ obj: obj });
                                    return;
                                }
                                scope.selected = obj;
                                scope.search = obj[scope.propertyToShow];
                                if (scope.modelType == undefined)
                                    ngModel.$setViewValue(obj[scope.modelProperty]);
                                else
                                    ngModel.$setViewValue(obj);

                                oldSelect = scope.search;
                                scope.open = false;
                            }

                            scope.nextPage = function () {
                                if (scope.currentPageNumber == scope.lastPageNumber)
                                    return;

                                scope.loading = true;
                                scope.currentPageNumber = parseInt(scope.currentPageNumber) + 1;
                                scope.onPageChange({ pageNumber: scope.currentPageNumber });


                                //on load complete
                                setTimeout(() => {
                                    scope.$apply(function () {
                                        scope.loading = false;
                                    });
                                }, 2000);

                            }

                            scope.previousPage = function () {
                                if (scope.currentPageNumber == 1)
                                    return;

                                scope.loading = true;
                                scope.currentPageNumber = parseInt(scope.currentPageNumber) - 1;
                                scope.onPageChange({ pageNumber: scope.currentPageNumber });


                                //on load complete
                                setTimeout(() => {
                                    scope.$apply(function () {
                                        scope.loading = false;
                                    });
                                }, 2000);

                            }

                            scope.focusOnInput = function () {
                                element.children('.xepos-input').children('input').focus();
                            }

                            scope.openDropdown = function () {
                                scope.open = true;
                                scope.search = "";
                                scope.keyboardIndex = -1;
                                let topOffset = element.offset().top;
                                //if (topOffset + 300 > window.innerHeight) {
                                //    scope.reverseOpen = true;
                                //}
                            }

                            scope.closeDropdown = function (e) {
                                setTimeout(function () {
                                    if (!$(document.activeElement).hasClass('next-page') && !$(document.activeElement).hasClass('previous-page')) {
                                        scope.$apply(function () {
                                            scope.open = false;
                                            scope.search = (oldSelect != undefined) ? oldSelect : scope.search;
                                        });
                                    }
                                    else {
                                        element.find('input').focus();
                                    }

                                }, 0);
                            }

                            element.on('keydown', function (event) {
                                if (scope.open && event.key == 'Enter')
                                    event.preventDefault();
                                switch (event.key) {
                                    case 'ArrowDown':
                                        if (scope.keyboardIndex < scope.repeat.length - 1)
                                            scope.keyboardIndex++;
                                        if (!scope.open)
                                            scope.openDropdown();
                                        break;
                                    case 'ArrowUp':
                                        if (scope.keyboardIndex > 0) scope.keyboardIndex--;
                                        break;
                                    case 'Enter':
                                        scope.selectThis(scope.repeat[scope.keyboardIndex]);
                                        break;
                                }
                                scope.$apply();
                            });
                        }
                    });
                }
            }
        }
    }
}
