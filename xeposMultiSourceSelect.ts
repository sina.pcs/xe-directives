﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {

            @Foundation.Core.DirectiveDependency({ name: 'xeposMultiSourceSelect' })
            export class xeposMultiSourceSelect implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'AE',
                        scope: { source1: '=', source2: '=', source3: '=', ngModel: '=', propertyToShow: '@', label: '@', title1: '@', title2: '@', title3: '@', modelProperty: '@', descriptionProperty1: '@', descriptionProperty2: '@', descriptionProperty3: '@', ngDisabled: '=', modelType: '@' },
                        template: function (elem, attrs: any) {
                            return `<div class="xepos-input" ng-class="{'md-input-has-value':selected != undefined,'not-valid':(${attrs.ngRequired} && !ngModel)}">
                                        <label>{{label}}</label>
                                        <input autocomplete="off" ng-disabled="ngDisabled" ng-model="search" ng-blur="closeDropdown()" ng-click="openDropdown()" ng-focus="openDropdown()" type="text"  />
                                        <div ng-if="clearable" class="clear" ng-show="!open && search" ng-click="clear()">
                                            <span class="sprite sprite-clear-icon"></span>
                                        </div>                                         
                                        <div class="dropdown" ng-class="{open:open}" ng-click="focusOnInput()">
                                            <span class="sprite sprite-dropdown-icon"></span>
                                        </div>                                                                           
                                    </div>
                                    <div class="row xepos-select-container" ng-show="open" >
                                        <div class="col" ng-class="{'col1of3':source3}">
                                            <div class="source-title">{{title1}}</div>
                                            <div ng-class="{reverse:reverseOpen,'no-label':label==''}" keyboard-index="{{keyboardIndex}}">
                                                <md-button ng-repeat="obj in source1 | filter : search" ng-mousedown="selectThis($event,obj,source1)" ng-class="{'keyboard-over':$index==keyboardIndex && keyboardSource == 1}">
                                                    {{obj[propertyToShow]}}
                                                    <span style="float:right" ng-if="descriptionProperty1">
                                                        {{obj[descriptionProperty1]}}
                                                    </span> 
                                                </md-button>
                                            </div>
                                            <div class="col-delimeter"></div>
                                        </div>
                                        <div class="col" ng-class="{'col1of3':source3}">
                                            <div class="source-title">{{title2}}</div>
                                            <div ng-class="{reverse:reverseOpen,'no-label':label==''}" keyboard-index="{{keyboardIndex}}">
                                                <md-button ng-repeat="obj in source2 | filter : search" ng-mousedown="selectThis($event,obj,source2)" ng-class="{'keyboard-over':$index==keyboardIndex && keyboardSource == 2}">
                                                    {{obj[propertyToShow]}}
                                                    <span style="float:right" ng-if="descriptionProperty2">
                                                        {{obj[descriptionProperty2]}}
                                                    </span> 
                                                </md-button>
                                            </div>
                                            <div class="col-delimeter"></div>
                                        </div>
                                        <div class="col col1of3">
                                            <div class="source-title">{{title3}}</div>
                                            <div ng-class="{reverse:reverseOpen,'no-label':label==''}" keyboard-index="{{keyboardIndex}}">
                                                <md-button ng-repeat="obj in source3 | filter : search" ng-mousedown="selectThis($event,obj,source3)" ng-class="{'keyboard-over':$index==keyboardIndex && keyboardSource == 3}">
                                                    {{obj[propertyToShow]}}
                                                    <span style="float:right" ng-if="descriptionProperty3">
                                                        {{obj[descriptionProperty3]}}
                                                    </span> 
                                                </md-button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="spacer"></div>`
                        },
                        link: function (scope: any, element: JQuery, attr: ng.IAttributes, ngModel: ng.INgModelController) {

                            if (scope.clearable == undefined)
                                scope.clearable = true;

                            scope.open = false;
                            scope.keyboardIndex = -1;
                            scope.keyboardSource = 1;
                            if (scope.modelProperty == undefined)
                                scope.modelProperty = 'Id';
                            if (scope.propertyToShow == undefined)
                                scope.propertyToShow = 'Name';

                            let dependencyManager = Foundation.Core.DependencyManager.getCurrent();
                            let $timeout = dependencyManager.resolveObject<ng.ITimeoutService>("$timeout");

                            let oldSelect;
                            scope.$watch(function () { return ngModel.$viewValue }, function (model) {
                                if (model == undefined || model == "" || model == null) {
                                    scope.search = "";
                                    oldSelect = scope.search;
                                    return;
                                }

                                $.each(scope.source1, function (key, value) {
                                    if (scope.modelType == 'object')
                                    {
                                        if (value == model) {
                                            scope.selectThis(null, value, scope.source1);
                                        }
                                    }
                                    else{
                                        if (value[scope.modelProperty] == model) {
                                            scope.search = value[scope.propertyToShow];
                                            oldSelect = scope.search;
                                        }
                                    }
                                })
                                $.each(scope.source2, function (key, value) {
                                    if (scope.modelType == 'object') {
                                        if (value == model) {
                                            scope.selectThis(null, value, scope.source2);
                                        }
                                    }
                                    else {
                                        if (value[scope.modelProperty] == model) {
                                            scope.search = value[scope.propertyToShow];
                                            oldSelect = scope.search;
                                        }
                                    }
                                })
                                if(scope.source3)
                                    $.each(scope.source3, function (key, value) {
                                        if (scope.modelType == 'object') {
                                            if (value == model) {
                                                scope.selectThis(null, value, scope.source3);
                                            }
                                        }
                                        else {
                                            if (value[scope.modelProperty] == model) {
                                                scope.search = value[scope.propertyToShow];
                                                oldSelect = scope.search;
                                            }
                                        }
                                    })
                            });

                            scope.clear = function () {
                                scope.search = "";
                                ngModel.$setViewValue("");
                                oldSelect = scope.search;
                                scope.open = false;
                            }
                            scope.selectThis = function (e: JQueryEventObject, obj: any, selectedSource: any) {
                                if (e) {
                                    if (angular.element(e.target).hasClass('edit-btn')) {
                                        scope.newButton({ obj: obj });
                                        return;
                                    }
                                }
                                scope.selected = obj;
                                scope.search = obj[scope.propertyToShow];
                                if (scope.modelType == undefined)
                                    ngModel.$setViewValue(obj[scope.modelProperty]);
                                else
                                    ngModel.$setViewValue(obj);
                                oldSelect = scope.search;
                                scope.open = false;
                            }

                            scope.focusOnInput = function () {
                                element.children('.xepos-input').children('input').focus();
                            }

                            scope.openDropdown = function () {
                                scope.open = true;
                                scope.search = "";
                                scope.keyboardIndex = -1;
                                let topOffset = element.offset().top;
                                //if (topOffset + 300 > window.innerHeight) {
                                //    scope.reverseOpen = true;
                                //}
                            }

                            scope.closeDropdown = function () {
                                setTimeout(() => {
                                    scope.$apply(function () {
                                        scope.open = false;
                                        scope.search = (oldSelect != undefined) ? oldSelect : scope.search;
                                    });
                                }, 100);
                            }

                            element.on('keydown', function (event) {
                                if (scope.open && event.key == 'Enter')
                                    event.preventDefault();
                                switch (event.key) {
                                    case 'ArrowDown':
                                        switch (scope.keyboardSource) {
                                            case 1:
                                                if (scope.keyboardIndex < scope.source1.length - 1)
                                                    scope.keyboardIndex++;
                                                if (!scope.open)
                                                    scope.openDropdown();
                                                break;
                                            case 2:
                                                if (scope.keyboardIndex < scope.source2.length - 1)
                                                    scope.keyboardIndex++;
                                                if (!scope.open)
                                                    scope.openDropdown();
                                                break;
                                            case 3:
                                                if (scope.keyboardIndex < scope.source3.length - 1)
                                                    scope.keyboardIndex++;
                                                if (!scope.open)
                                                    scope.openDropdown();
                                                break;
                                        }
                                        break;
                                    case 'ArrowUp':
                                        if (scope.keyboardIndex > 0) scope.keyboardIndex--;
                                        break;
                                    case 'ArrowLeft':
                                        if (scope.keyboardSource > 1) {
                                            scope.keyboardSource -= 1;
                                            scope.keyboardIndex = 0;
                                        }
                                        break;
                                    case 'ArrowRight':
                                        let numberOfSources = scope.source3 ? 3 : 2;
                                        if (scope.keyboardSource < numberOfSources) {
                                            scope.keyboardSource += 1;
                                            scope.keyboardIndex = 0;
                                        }
                                        break;
                                    case 'Enter':
                                        switch (scope.keyboardSource) {
                                            case 1:
                                                scope.selectThis(null, scope.source1[scope.keyboardIndex], scope.source1);
                                                break;
                                            case 2:
                                                scope.selectThis(null, scope.source2[scope.keyboardIndex], scope.source2);
                                                break;
                                            case 3:
                                                scope.selectThis(null, scope.source2[scope.keyboardIndex], scope.source3);
                                                break;
                                        }

                                        break;
                                }
                                scope.$apply();
                            });
                        }
                    });
                }
            }
        }
    }
}
