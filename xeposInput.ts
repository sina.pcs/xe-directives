﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {

            @Foundation.Core.DirectiveDependency({ name: 'dec' })
            export class x {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'A',
                        scope: false,
                        link: function (scope: any, element: any, attrs: any, ngModel: ng.INgModelController) {
                            ngModel.$parsers.push(function (viewValue) {
                                if (viewValue.startsWith('.')) {
                                    viewValue = '0' + viewValue;
                                }
                                if (viewValue.endsWith('.')) {
                                    viewValue = viewValue + '0';
                                }
                                return viewValue;
                            });
                        }
                    });
                }
            }

            @Foundation.Core.Injectable()
            export class XeposInputController {
            }
            @Foundation.Core.DirectiveDependency({ name: 'xeposInput' })
            export class xeposInput implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'E',
                        controller: XeposInputController,
                        scope: { label: '@', ngModel: '=', ngDisabled: '=', ngChange: "&", ngBlur: "&" },
                        template: function (elem: any, attrs: any) {
                            let customNgModelOption = "";
                            if (attrs.type == 'decimal')
                                customNgModelOption = `ng-model-options="{updateOn: 'blur'}"`;

                            let maxLenght = attrs.maxlength ? `maxlength="${attrs.maxlength}"` : "";

                            return `<div class="xepos-input">
                                        <label>{{label}}</label>
                                        <input dec type='${attrs.type}' ${maxLenght} ${customNgModelOption} ng-model='ngModel' ng-disabled='ngDisabled' ng-blur='ngBlur()' />
                                    </div>`;
                        },
                        link: function (scope: any, element: any, attrs: any, ngModel: ng.INgModelController) {
                            //scope.onChange = function () {
                            //    ngModel.$setViewValue(scope.value);
                            //};
                            //ngModel.$render = function () {
                            //    scope.value = ngModel.$modelValue;
                            //};
                            scope.$watch('ngModel', function (newValue, oldValue) {
                                if ((!newValue && oldValue) || newValue) {
                                    setTimeout(scope.ngChange);
                                }
                            });
                        }
                    });
                }
            }

            @Foundation.Core.DirectiveDependency({ name: 'xeposInputNumber' })
            export class xeposInputNumber implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        restrict: 'A',
                        link: function (scope: any, element: any, attrs: any) {
                            element.on('keydown', function (e) {
                                if (!((e.keyCode < 58 && e.keyCode > 47) || (e.keyCode > 95 && e.keyCode < 106) || e.keyCode == 110 || e.keyCode == 190 || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39)) {
                                    e.preventDefault();
                                }
                                else {
                                    if (attrs.isInteger == "true" && (e.keyCode == 110 || e.keyCode == 190 )) {
                                        e.preventDefault();
                                    }
                                }
                            });
                        }
                    });
                }
            }
        }

        // <xepos-input label="{{::'CostPriceLabel' | translate}}"><input name="CostPrice" ng-model="vm.product.CostPrice" ng-required="true" /></xepos-input>

        //@Foundation.Core.DirectiveDependency({ name: "xeposInput" })
        //export class XeposInput implements Foundation.ViewModel.Contracts.IDirective {
        //    public getDirectiveFactory(): ng.IDirectiveFactory {
        //        return () => ({
        //            transclude: true,
        //            scope: { label: '@' },
        //            template: (element: JQuery, attrs: ng.IAttributes) => {
        //                return `<div class="xepos-input">
        //                                <label>{{label}}</label>
        //                                <input-container xepos-input-transclude></input-container>
        //                           </div>`;
        //            }
        //        });
        //    }
        //}

        //@Foundation.Core.DirectiveDependency({ name: "xeposInputTransclude" })
        //export class XeposInputTransclude implements Foundation.ViewModel.Contracts.IDirective {
        //    public getDirectiveFactory(): ng.IDirectiveFactory {
        //        return () => ({
        //            link: ($scope: ng.IScope, element: JQuery, attrs: ng.IAttributes, ctrl: ng.INgModelController, transclude: ng.ITranscludeFunction) => {
        //                transclude((clone, transcludeScope) => {
        //                    angular.element(element).append(clone);
        //                });
        //            }
        //        });
        //    }
        //}
    }
}
