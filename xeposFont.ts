﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {
            @Foundation.Core.Injectable()
            export class XeposFontController {
            }
            @Foundation.Core.DirectiveDependency({ name: 'xeposFont' })
            export class xeposFont implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'AE',
                        controller: XeposFontController,
                        scope: { label: '@', ngModel: '=', ngDisabled: '=', ngChange: "&"},
                        template: function (elem: any, attrs) {
                            return `<div class="xepos-font">
                                        <label>{{label}}</label>
                                        <div class="buttons">
                                            <button ng-disabled="ngDisabled" ng-click="decrease()">-</button>
                                            <input ng-disabled="ngDisabled" ng-model="ngModel"></input>
                                            <button ng-disabled="ngDisabled" ng-click="increase()">+</button>
                                        </div>
                                    </div>`;
                        },
                        link: function (scope: any, element: any, attrs: any, ngChange: "&", ngModel: ng.INgModelController) {
                            scope.increase = function () {
                                scope.ngModel = parseInt(scope.ngModel) + 1;
                                //ngModel.$setViewValue(v);
                            }
                            scope.decrease = function () {
                                if (scope.ngModel > 0) {
                                    scope.ngModel = parseInt(scope.ngModel) - 1;
                                    //ngModel.$setViewValue(v);
                                }
                            }

                            scope.$watch('ngModel', function (newValue, oldValue) {
                                if ((!newValue && oldValue) || newValue) {
                                    if (newValue == null)
                                        scope.ngModel = 0;
                                    setTimeout(scope.ngChange);
                                }
                            });
                        }
                    });
                }
            }
        }
    }
}
