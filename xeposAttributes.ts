﻿/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.core/foundation.core.d.ts" />
/// <reference path="../../../xeposcdnserver/wwwroot/bower_components/bit-releases/foundation.viewmodel/foundation.viewmodel.d.ts" />

module XBack {
    export module View {
        export module Directives {


            @Foundation.Core.DirectiveDependency({ name: 'xeposAttributes' })
            export class xeposAttributes implements Foundation.ViewModel.Contracts.IDirective {
                public getDirectiveFactory(): ng.IDirectiveFactory {
                    return () => ({
                        require: "?ngModel",
                        restrict: 'AE',
                        scope: { ngModel: '=', label: '@', name: '@', keyup: '&' },
                        template: function (elem, attrs:any) {
                            return `<div class="values" ng-click="focus()" ng-class="{focused:focused}"> 
                                        <span ng-repeat="value in values" class="xepos-chip" ng-show="!value.IsDeleted">{{value.Name}}<i ng-click="deleteValue($event,value)" class="sprite sprite-chip-X"></i></span>
                                        <input placeholder="{{label}}" autocomplete="off" type="text" ng-blur="focused=false" />                                    
                                    </div>`
                        },
                        link: function (scope: any, element: JQuery, attr: ng.IAttributes, ngModel: ng.INgModelController) {
                            
                          if (scope.propertyToShow == undefined)
                                scope.propertyToShow = 'Name';

                            let dependencyManager = Foundation.Core.DependencyManager.getCurrent();
                            let $timeout = dependencyManager.resolveObject<ng.ITimeoutService>("$timeout");

                            let oldSelect;
                            scope.values = [];
                            scope.focused = false;

                            $timeout(() => {

                                let unRegister = scope.$watch(function () { return ngModel.$viewValue }, function (model) {
                                    if (model == undefined)
                                        return;
                                    
                                    unRegister();
                                    if (!scope.New) {
                                        scope.values = model;
                                    }
                                });
                            });

                            scope.focus = function () {
                                scope.focused = true;
                                element.find('input').focus();
                            }
                            scope.selectThis = function (obj: Object) {

                                scope.selected = obj;
                                let model = [];
                                if (ngModel.$modelValue) {
                                    model = ngModel.$modelValue;
                                }
                                else {
                                    scope.New = true;
                                }

                                model.push(obj);

                                ngModel.$setViewValue(model);
                            }

                            scope.deleteValue = function (event,value: ServerCodes.Model.Dto.AttributeValueDto) {
                                
                                let index = scope.values.indexOf(value);
                                angular.element(event.target).parent('span').addClass('isGoingToBeDeleted');
                                
                                $timeout(() => {
                                  
                                    value.IsDeleted = true;

                                }, 300);
                            }

                            element.on('keydown', function (event: any) {
                                if (event.key == 'Enter', event.key == ',')
                                    event.preventDefault();
                                switch (event.key) {
                                    case ',':
                                    case 'Enter':
                                        let val: string = event['target'].value;
                                        if (val == '' || val == null)
                                            break;
                          
                                        let findInValues = scope.values.find((v, i) => v.Name.toLowerCase() == val.toLowerCase());
                                        
                                        if (findInValues && !findInValues.IsDeleted) {
                                            let index = scope.values.indexOf(findInValues);
                                            angular.element(event.target).parent('.values').children('.xepos-chip:nth-child(' + (index + 1) + ')').addClass('shake');
                                            $timeout(() => {
                                                angular.element('.values span.shake').removeClass('shake');
                                            }, 400);
                                        }
                                        else {
                                            scope.$applyAsync(function () {
                                                scope.keyup({ val: val }).then((data) => {

                                                    scope.selectThis(data);
                                                });
                                            });
                                        }

                                        angular.element(event.target).val('');
                                        break;

                                    default:
                                        break;
                                }
                            });
                        }
                    });
                }
            }
        }
    }
}
